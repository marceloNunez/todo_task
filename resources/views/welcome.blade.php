<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

    </style>
</head>

<body>


    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="content mt-4">
                    <div class="title m-b-md">
                        <h1>Crud Todo Task</h1>
                    </div>
                    <div class="ibox-title">
                        @include('partials.messages')
                    </div>
                    <div>
                        <form method="post" action="{{ action('ChoresController@store') }}">
                            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Example textarea</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
                                    name="description"></textarea>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Example select</label>
                                    <select class="form-control" id="exampleFormControlSelect1" name="task_state_id">
                                        <option>Select State Task</option>

                                        @foreach ($taskStates as $item)

                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-dark">create</button>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="content mt-4">
                    <div class="title m-b-md">
                        <h1>Laravel Todo Task</h1>
                    </div>
                    <div>
                        <ul class="list-group list-group-flush">
                            @foreach ($chores as $item)
                                <li class="list-group-item text-left">{{ 
                                $item->description }} <span class="badge badge-primary">{{$item->task_state->name}}</span>
                                    <div class=" float-right">
                                         
                                        <form id="delete-form" method="POST" action="{{url('task/'.$item->id)}}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-danger" value="remove">
                                            </div>
                                        </form>
                                        <a href="" class="btn  btn-success">update</a>
                                </li>
                             @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>

</body>

</html>
