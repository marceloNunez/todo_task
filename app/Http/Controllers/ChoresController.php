<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Chore;
use App\Task_state;

class ChoresController extends Controller
{
    //
    public function index()
    {
        $chores = Chore::all();
        $taskStates = Task_state::all();
        return view('welcome', compact('chores', 'taskStates'));
    }

    /**
     * show view form new category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store data new category
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'description' => 'required',
            'task_state_id' => 'required',
        ]);
        if ($v && $v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());
        } else {
            $fields = $request->all();
            $chore = Chore::create($fields);
            Session::flash('flash_message', 'Chore created!');
            Session::flash('flash_message_type', 'success');
        }
        return redirect('/');
    }

    /**
     * show view form edit category
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $chore = Chore::find($id);
        return view('admin.categories.edit', compact('chore'));
    }

    /**
     * Store data category edit
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($v && $v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());
        } else {

            $chore = Chore::find($id);
            $chore->fill($request->all());

            $chore->save();

            Session::flash('flash_message', 'Chore updated!');
            Session::flash('flash_message_type', 'success');
        }
        return redirect('admin/categories');
    }

    /**
     * delete category
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $chore = Chore::findOrFail($id);
        if ($chore) {
            $chore->delete();
            Session::flash('flash_message', 'Chore deleted!');
            Session::flash('flash_message_type', 'success');
        } else {
            Session::flash('flash_message', 'Chore not found!');
            Session::flash('flash_message_type', 'warning');
        }
        return redirect('/');
    }
}
