<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task_state;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Task_stateController extends Controller
{
    //
    public function index()
    {
        $taskState = Task_state::all();
        return view('admin.categories.index', compact('taskState'));
    }

    /**
     * show view form new category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('');
    }

    /**
     * Store data new category
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($v && $v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());
        } else {
            $fields = $request->all();
            $taskState = Task_state::create($fields);

            Session::flash('flash_message', 'Task State created!');
            Session::flash('flash_message_type', 'success');
        }
        return redirect('');
    }

    /**
     * show view form edit category
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $taskState = Task_state::find($id);
        return view('', compact('taskState'));
    }

    /**
     * Store data category edit
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($v && $v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());
        } else {

            $taskState = Task_state::find($id);
            $taskState->fill($request->all());

            $taskState->save();

            Session::flash('flash_message', 'Task State updated!');
            Session::flash('flash_message_type', 'success');
        }
        return redirect('');
    }

    /**
     * delete category
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $taskState = Task_state::findOrFail($id);
        if ($taskState) {
            $taskState->delete();
            Session::flash('flash_message', 'Task State deleted!');
            Session::flash('flash_message_type', 'success');
        } else {
            Session::flash('flash_message', 'Task State not found!');
            Session::flash('flash_message_type', 'warning');
        }
        return redirect('');
    }
}
