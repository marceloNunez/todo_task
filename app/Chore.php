<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chore extends Model
{
    //Params
    protected $table = 'chores';
    protected $fillable = ['description', 'task_state_id'];
    //Relations
    public function task_state()
    {
        return $this->belongsTo('App\Task_state');
    }
}
