<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task_state extends Model
{
    //Params
    protected $table = 'task_states';
    protected $fillable = ['name'];

    //Relations
    public function chores()
    {
        return $this->hasMany('App\Chore');
    }
}
