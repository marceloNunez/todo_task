<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTaskStateId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('chores', function (Blueprint $table) {
            $table->integer('task_state_id')->unsigned()->nullable()->default(null);
            $table->foreign('task_state_id')->references('id')->on('task_states')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('chores', function (Blueprint $table) {
            $table->dropForeign('chores_task_state_id_foreign');
            $table->dropColumn('task_state_id');
        });
    }
}
